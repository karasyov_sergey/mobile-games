package com.mygdx.screens;

import com.badlogic.gdx.Screen;
import com.mygdx.game.Drop;

import com.mygdx.render.GameRender;
import com.mygdx.render.GameWorld;


public class GameScreen implements Screen {
    private GameRender gameRender;

    GameScreen(final Drop game) {
        GameWorld gameWorld = new GameWorld();
        this.gameRender = new GameRender(game, gameWorld);

    }


    @Override
    public void render(float delta) {
        gameRender.render();
    }

    @Override
    public void resize(int width, int height) {
    }

    @Override
    public void show() {
    }

    @Override
    public void hide() {
    }

    @Override
    public void pause() {
    }

    @Override
    public void resume() {
    }

    @Override
    public void dispose() {

    }


}
