package com.mygdx.render;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.TimeUtils;
import com.mygdx.game.Drop;

public class GameWorld {
    Texture dropImage;
    Texture bucketImage;
    Rectangle bucket;
    Array<Rectangle> raindrops;
    private long lastDropTime;
    static long spawnRaindropTime;

    public GameWorld() {
        spawnRaindropTime = 1_000_000_000;
        dropImage = new Texture(Gdx.files.internal("droplet.png"));
        bucketImage = new Texture(Gdx.files.internal("bucket.png"));
        bucket = new Rectangle();
        bucket.width = bucketImage.getWidth();
        bucket.height = bucketImage.getHeight();
        bucket.x = Drop.WIDTH / 2 - bucket.width / 2;

        bucket.y = 20;

        raindrops = new Array<>();
        spawnRaindrop();
    }

    private void spawnRaindrop() {
        Rectangle raindrop = new Rectangle();
        raindrop.x = MathUtils.random(0, 800 - 64);
        raindrop.y = Drop.HEIGHT;
        raindrop.width = dropImage.getWidth();
        raindrop.height = dropImage.getHeight();
        raindrops.add(raindrop);
        lastDropTime = TimeUtils.nanoTime();
    }

    void update() {
        if (TimeUtils.nanoTime() - lastDropTime > spawnRaindropTime)
            spawnRaindrop();
    }


    void dispose() {
        bucketImage.dispose();
        dropImage.dispose();
    }
}
