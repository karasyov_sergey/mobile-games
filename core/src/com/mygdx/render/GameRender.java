package com.mygdx.render;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;
import com.mygdx.game.Drop;

import java.util.Iterator;

import com.mygdx.screens.GameOverScreen;

public class GameRender {
    private Drop game;
    private GameWorld gameWorld;
    private OrthographicCamera camera;
    private int level;
    private int levelCounter;
    private int dropsGathered;
    private Vector3 touchPos;
    private int lives;

    public GameRender(Drop game, GameWorld gameWorld) {
        this.game = game;
        this.gameWorld = gameWorld;
        this.camera = new OrthographicCamera();
        this.touchPos = new Vector3();
        this.lives = 5;
        this.level = 1;
        this.dropsGathered = 0;
        this.levelCounter = 0;
        camera.setToOrtho(false, Drop.WIDTH, Drop.HEIGHT);
    }

    public void render() {
        Gdx.gl.glClearColor(0, 0, 0.2f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        camera.update();

        game.batch.setProjectionMatrix(camera.combined);

        game.batch.begin();
        game.font.draw(game.batch, "Drops Collected: " + dropsGathered, 10, 480);
        game.font.draw(game.batch, "lives left " + lives, 10, 440);
        game.font.draw(game.batch, "level " + level, 10, 400);
        game.batch.draw(gameWorld.bucketImage,
                gameWorld.bucket.x, gameWorld.bucket.y);
        for (Rectangle raindrop : gameWorld.raindrops) {
            game.batch.draw(gameWorld.dropImage,
                    raindrop.x, raindrop.y);
        }
        game.batch.end();
        gameWorld.update();

        if (gameWorld.bucket.x < 0)
            gameWorld.bucket.x = 0;
        if (gameWorld.bucket.x > Drop.WIDTH - gameWorld.bucket.width)
            gameWorld.bucket.x = Drop.WIDTH - gameWorld.bucket.height;

        if (Gdx.input.isTouched()) {
            touchPos.set(Gdx.input.getX(), Gdx.input.getY(), 0);
            camera.unproject(touchPos);
            gameWorld.bucket.x = touchPos.x - gameWorld.bucket.width / 2;
        }

        Iterator<Rectangle> iterator = gameWorld.raindrops.iterator();
        while (iterator.hasNext()) {
            Rectangle raindrop = iterator.next();
            raindrop.y -= 200 * Gdx.graphics.getDeltaTime();
            if (raindrop.y + raindrop.height < 0) {
                iterator.remove();
                lives--;
            }
            if (raindrop.overlaps(gameWorld.bucket)) {
                dropsGathered++;
                levelCounter++;
                iterator.remove();
            }
        }

        if (levelCounter % 15 == 0 && levelCounter != 0 && level <= 15) {
            level++;
            GameWorld.spawnRaindropTime -= 50_000_000;
            levelCounter = 0;
        }

        if (lives == 0) {
            gameWorld.dispose();
            game.setScreen(new GameOverScreen(game));
        }
    }
}
